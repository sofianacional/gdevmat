void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
          0, 0, 0, 
          0, -1, 0);
  background(0);
}

 Vector2 mousePos()
 {
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
 }

float dtAngle = 1.5;
float dtR = 200;
float dtG = 150;
float dtB = 100;

float angle;
void draw()
{
  background(0);
  
  Vector2 mouse = mousePos();
  mouse.normalize().mult(350);
  
  //Noise rotation
   rotate(angle);
   angle = map(noise(dtAngle), 0, 1, 0, 20);
 
  
  strokeWeight(7); // white
  stroke(255, 255, 255);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  mouse.div(0.98); // glow
  strokeWeight(20);
  stroke(map(noise(dtR), 0, 1, 50, 255), map(noise(dtG), 0, 1, 50, 255), map(noise(dtB), 0, 1, 50, 255), 100);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  mouse.div(8); // middle part
  strokeWeight(12);
  stroke(100, 100, 100);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  dtAngle += 0.01f;
  dtR += 0.01f;
  dtG += 0.01f;
  dtB += 0.01f;
  //println(mouse.mag());
}
