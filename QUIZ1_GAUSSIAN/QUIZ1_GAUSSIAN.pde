// GAUSSIAN = normal distribution
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
          0, 0, 0, 
          0, -1, 0);
  background(0);
}

void draw()
{
  if(frameCount % 1000 == 0)
  {
    background(0);
  }
  
  float gauss = randomGaussian();
  float standardDeviation = 400;
  float mean = 100;
  
  float xPos = (standardDeviation * gauss) + mean;
  float yPos = floor(random(-280, 280));
  
  float sizeGauss = randomGaussian();
  float sizeSD = 50;
  float sizeMean = 10;
  float size = (sizeSD * sizeGauss) + sizeMean;
  
  noStroke();
  fill(floor(random(255)), floor(random(255)), floor(random(255)), 63);
  circle(xPos, yPos, size);
}
