Mover mover;
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
          0, 0, 0, 
          0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

void draw()
{
  background(255);
  fill(200, 0, 0);
  mover.render();
 
    if(mover.position.x >= 0)
    {
      mover.acceleration = new PVector(-0.2, 0);
      
      if (mover.velocity.x <= 0)
      {
        mover.velocity = new PVector(0, 0);
        mover.acceleration = new PVector(0, 0);
      }
    }
}
