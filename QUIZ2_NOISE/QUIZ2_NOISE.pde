// NOISE = min/max (range)
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
          0, 0, 0, 
          0, -1, 0);
  background(0);
}

float dt = 1;
float dt2 = 1.5;

float dtR = 200;
float dtG = 150;
float dtB = 100;

void draw()
{
  float Xpos = noise(dt);
  float Ypos = noise(dt2);
  
  float x = map(Xpos, 0, 1, Window.left, Window.right);
  float y = map(Ypos, 0, 1, Window.top, Window.bottom);
  float size = map(noise(dt), 0, 1, 50, 70);
  //rect(Window.left + (dt * 100), Window.bottom, 1, x);
  
  //color
  noStroke();
  fill(map(noise(dtR), 0, 1, 50, 255), map(noise(dtG), 0, 1, 50, 255), map(noise(dtB), 0, 1, 50, 255), 80);
  circle(x, y, size);
  
  dt += 0.01f;
  dt2 += 0.01f;
  dtR += 0.01f;
  dtG += 0.01f;
  dtB += 0.01f;
}
