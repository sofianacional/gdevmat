//Mover mover = new Mover(0, 200);
Mover[] mover = new Mover[10];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);


void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for(int i = 0; i < mover.length; i++)
  {
    mover[i] = new Mover();
    mover[i].position.y = Window.top + 50;
    mover[i].position.x = (Window.widthPx/mover.length) * (i+1) - Window.right - 50;
    mover[i].mass = i;
    
    mover[i].acceleration = new PVector(0.1, 0);
    
    mover[i].setColor(random(1, 255), random(1, 255), random(1, 255), random(150, 255));
  }
  
}


void draw()
{
  background(255);
  
  ocean.render();
  noStroke();

for (Mover m : mover)
  {
    m.applyGravity();
    m.applyFriction();

    
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(m))
    {
      m.applyForce(ocean.calculateDragForce(m)); 
    }
  }
 
}
