class Walker
{
  float xPosition;
  float yPosition;
  
  void render()
  {
    fill(floor(random(255)), floor(random(255)), floor(random(255)), 63);
    noStroke();
    circle(xPosition, yPosition, 30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if (decision == 0)
    {
      yPosition += 10;
    }
    else if (decision == 1)
    {
      yPosition -= 10;
    }
    else if (decision == 2)
    {
      xPosition += 10;
    }
     else if (decision == 3)
    {
      xPosition -= 10;
    }
    else if (decision == 4) // down left 
    {
      xPosition -= 10;
      yPosition -= 10;
    }
    else if (decision == 5) // up right
    {
      xPosition += 10;
      yPosition += 10;
    }
     else if (decision == 6) // up left
    {
      xPosition -= 10;
      yPosition += 10;
    }
      else if (decision == 7) // down right
    {
      xPosition += 10;
      yPosition -= 10;
    }
  }
  
}
