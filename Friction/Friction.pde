Mover[] mover = new Mover[5];
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
    0, 0, 0, 
    0, -1, 0);
  background(0);

  for (int i = 0; i < mover.length; i++)
  {
    mover[i] = new Mover();
    mover[i].position.x = Window.left + 50;
    mover[i].position.y = (Window.heightPx/mover.length) * (i+1) - Window.top - 50;
    float rand = random(10, 80);
    mover[i].mass = rand;
    mover[i].scale = rand;

    mover[i].acceleration = new PVector(0.1, 0);

    mover[i].setColor(random(1, 255), random(1, 255), random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1f, 0);

void draw()
{
  background(255);

  noStroke();
  for (Mover m : mover)
  {
    m.render();

    m.update();

    if (m.position.x >= 0)
      m.applyFriction();
    else
      m.applyForce(wind);
      
    if(frameCount % 1000 == 0)
       reset();
  }

}

void reset()
{
  frameCount = -1;
}
