public class Mover
{
  //float x, y;
  public PVector position = new PVector(Window.left + 50, 0);
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass;
  public float scale;
  
  
  public float r = 255, g = 255, b = 255, a = 255;
  
  public void render()
  {
    noStroke();
    fill(r, g, b, a);
    update();
    circle(position.x, position.y, scale);
  }
  
  public void setColor(float r, float g, float b, float a)
  {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  
  public void update()
   {
      // we add the acceleration to the velocity every frame
      this.velocity.add(this.acceleration);
      
      this.velocity.limit(mass);
     
      //we add the velocity to the position every frame
      this.position.add(this.velocity);
      
      this.acceleration.mult(0);
   }
   
   
   public void applyForce(PVector force)
   {
     PVector f = PVector.div(force, this.mass); // F = M / A
     this.acceleration.add(f); // accumulate force every frame
   }
   
   public void applyFriction()
   {
      float c = 0.1; //coefficient of friction
      float normal = 1;
      float frictionMag = c * normal;
      PVector friction = this.velocity.copy(); //gets the copy of the mover's velocity so its value won't be modified
      friction.mult(-1);
      friction.normalize();
      friction.mult(frictionMag);
      applyForce(friction);
 }
}
