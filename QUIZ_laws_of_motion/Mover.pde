public class Mover
{
  //float x, y;
  public PVector position = new PVector(Window.left + 50, 0);
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass = 1;
  public float scale = 50;
  
  
  public float r = 255, g = 255, b = 255, a = 255;
  
  public void render()
  {
    noStroke();
    fill(r, g, b, a);
    update();
    circle(position.x, position.y, scale);
  }
  
  public void setColor(float r, float g, float b, float a)
  {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  
  private void update()
  {
      this.velocity.add(this.acceleration);
      this.velocity.limit(20);
      this.position.add(this.velocity);
      this.acceleration.mult(0);
  }
  
  public void applyForce(PVector force)
  {
    PVector f = PVector.div(force, mass);
    this.acceleration.add(f);
  }
}
