Mover[] mover = new Mover[10];
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
    0, 0, 0, 
    0, -1, 0);
  background(0);

  for (int i = 0; i < mover.length; i++)
  {
    mover[i] = new Mover();
    mover[i].mass = i;
    mover[i].scale = i*10;
    mover[i].setColor(random(1, 255), random(1, 255), random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1, 0);
PVector gravity = new PVector(0, -1);


void draw()
{
  background(255);
  fill(200, 0, 0);
  //mover.render();

  for (int i = 0; i < mover.length; i++)
    mover[i].render();

  for (int i = 0; i < mover.length; i++)
  {
    mover[i].applyForce(wind);
    mover[i].applyForce(gravity);

    if (mover[i].position.y < Window.bottom)
    {
      mover[i].velocity.y *= -1;
      mover[i].position.y = Window.bottom;
    }

    if (mover[i].position.x > Window.right)
    {
      mover[i].velocity.x *= -1;
      mover[i].position.x = Window.right;
    }

    if (mover[i].position.x < Window.left)
    {
      mover[i].velocity.x *= -1;
      mover[i].position.x = Window.left;
    }
  }
}
