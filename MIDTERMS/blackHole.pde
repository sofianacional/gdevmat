class blackHole
{
  float x, y;
  public PVector blackholePos = new PVector();
  
  public void render()
  {
    fill(255, 255, 255);
    noStroke();
    circle(x, y, 50);
  }
  
  public void position()
  {
    x = floor(random(-280, 280));
    y = floor(random(-280, 280));
    blackholePos = new PVector (x, y);
  }
  
}
