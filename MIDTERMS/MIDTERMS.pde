void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30/180), 
          0, 0, 0, 
          0, -1, 0);
  background(0);
  
  for(int i = 0; i < 100; i++)
      matter[i] = new Matter();
  for(int i = 0; i < 100; i++)
      matter[i].spawn();
}

  blackHole blackHole = new blackHole();
  Matter[] matter = new Matter[100];

void draw()
{
  background(0);
  
   for(int i = 0; i < 100; i++)
     matter[i].render();
   
   for(int i = 0; i < 100; i++) // next position
     matter[i].direction(blackHole.blackholePos);
     
  blackHole.render();
     
  if(frameCount % 200 == 0)
  {
    blackHole.position();
    
    for(int i = 0; i < 100; i++)
      matter[i].spawn();
  }
  
}
