class Matter
{
  public PVector position = new PVector();
  public float x, y;
  color col = color(floor(random(255)), floor(random(255)), floor(random(255)), 170);
  float size = random(20, 45);
  int speed = 6;
  public void render()
  {
    fill(col);
    noStroke();
    circle(position.x, position.y, size);
  }
  
  public void spawn()
  {
    float gauss = randomGaussian();
    float standardDeviation = 400;
    float mean = 100;
    
    position = new PVector((standardDeviation * gauss) + mean, floor(random(Window.bottom, Window.top)));
  
  }
  
  public void direction(PVector blackholePos)
  {
    PVector correctDirection = PVector.sub(blackholePos, position).normalize().mult(speed);
    position.add(correctDirection);
  }
  
}
